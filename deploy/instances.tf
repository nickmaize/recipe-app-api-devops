# # Retrieve latest AMI owned by Amazon with the value containing a wildcard (*)
# data "aws_ami" "amazon_linux" {
#   most_recent = true
#   filter {
#     name   = "name"
#     values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
#   }
#   owners = ["amazon"]
# }
# #########
# # DC IAM Role
# #########
# resource "aws_iam_role" "dc" {
#   name               = "${local.prefix}-dc"
#   assume_role_policy = file("./templates/EC2/instance-profile-policy.json")
#   tags               = local.common_tags
# }
# #########
# # DC IAM Role : Attach Existing Policy
# #########
# resource "aws_iam_role_policy_attachment" "dc_attach_policy" {
#   role       = aws_iam_role.dc.name
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
# }
# #########
# # DC Instance Profile 
# # --> FYI: Can't attach IAM Role directly to Instance
# #########
# resource "aws_iam_instance_profile" "dc" {
#   name = "${local.prefix}-dc-instance-profile"
#   role = aws_iam_role.dc.name
# }
# #########
# # DC Instance
# #########
# resource "aws_instance" "dc" {
#   ami                  = "ami-075aca13d95c4c83a" # data.aws_ami.amazon_linux.id
#   instance_type        = "t2.micro"
#   subnet_id            = aws_subnet.public_a.id
#   private_ip           = "10.192.10.185"
#   iam_instance_profile = aws_iam_instance_profile.dc.name
#   user_data            = file("./templates/EC2/windows-user-data.txt")
#   key_name             = var.instance_key_name
#   vpc_security_group_ids = [
#     aws_security_group.dc.id
#   ]

#   # Execute Powershell inline
#   provisioner "remote-exec" {
#     connection {
#       host     = self.public_ip
#       type     = "winrm"
#       user     = var.win_username
#       password = var.win_password
#       timeout  = "10m"
#       https    = false
#       #   agent    = false
#       insecure = "true"
#     }
#     inline = [
#       "echo Y Y Y  ${self.public_ip} 255.255.255.0 172.17.131.1 nickdomain nickdomain.army.SMIL.MIL ${var.domain_recovery_password} ${var.domain_recovery_password} Y | powershell.exe -File C:\\Scripts\\ADinstall-2012-1.ps1"
#     ]
#   }
#   # Merge allows you to pull in common_tags from locals
#   # w/map allowing additional custom tags
#   tags = merge(
#     local.common_tags,
#     map("Name", "${local.prefix}-dc")
#   )
# }
# #########
# # DC Security Group
# #########
# resource "aws_security_group" "dc" {
#   name        = "${local.prefix}-dc-sg"
#   description = "security group that allows http, rdp and winrm and all egress traffic"
#   vpc_id      = aws_vpc.main.id
#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   # http - Port 80
#   ingress {
#     from_port   = 80
#     to_port     = 80
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
#   # rdp - Port 3389
#   ingress {
#     from_port   = 3389
#     to_port     = 3389
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   # WinRM - Port 5985 
#   ingress {
#     from_port   = 5985
#     to_port     = 5985
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   # WinRM - Port 5986 
#   ingress {
#     from_port   = 5986
#     to_port     = 5986
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   tags = local.common_tags
# }

#########
# CentOS Runner Instance
#########
# resource "aws_instance" "CentOsRunner" {
#   ami           = "ami-0affd4508a5d2481b" # data.aws_ami.amazon_linux.id
#   instance_type = "t2.micro"
#   subnet_id     = "aws_subnet.public_a.id"
#   # private_ip    = "10.192.10.180"
#   # iam_instance_profile = aws_iam_instance_profile.dc.name
#   # user_data = templatefile("./templates/EC2/CentOsRunner.sh", local.runner_vars)
#   key_name = "macbook"
#   vpc_security_group_ids = [
#     "sg-06e3de0413210749a"
#   ]

#   # # Execute Powershell inline
#   # provisioner "remote-exec" {
#   #   connection {
#   #     host     = self.public_ip
#   #     type     = "winrm"
#   #     user     = var.win_username
#   #     password = var.win_password
#   #     timeout  = "10m"
#   #     https    = false
#   #     #   agent    = false
#   #     insecure = "true"
#   #   }
#   #   inline = [
#   #     "echo Y Y Y  ${self.public_ip} 255.255.255.0 172.17.131.1 nickdomain nickdomain.army.SMIL.MIL ${var.domain_recovery_password} ${var.domain_recovery_password} Y | powershell.exe -File C:\\Scripts\\ADinstall-2012-1.ps1"
#   #   ]
#   # }
#   # Merge allows you to pull in common_tags from locals
#   # w/map allowing additional custom tags
#   tags = merge(
#     local.common_tags,
#     map("Name", "${local.prefix}-dc")
#   )
# }