# Official image for Hashicorp's Terraform. It uses light image which is Alpine
# based as it is much lighter.
# https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Terraform.gitlab-ci.yml
# Entrypoint is also needed as image by default set `terraform` binary as an
# entrypoint.
image:
  name: registry.gitlab.com/nickmaize/recipe-app-api-devops:latest
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

stages:
  - Validate
  - TST Plan
  - TST Apply
  - TST Destroy

Validate Terraform:
  stage: Validate
  script:
    - echo "Validate Terraform"
    # Each time the job runs, GitLab clones project to the container
    - cd deploy/
    # Checks on custom image
    - terraform --version
    - ansible --version
    # Backend not needed for validate and formatting, no changes will be made in AWS
    - terraform init --backend=false
    - terraform validate
    # Format Terraform code, -check will fail the job if code is not properly formatted.
    - terraform fmt -check
  environment:
    name: prod
  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master" || $CI_COMMIT_BRANCH == "master"'
    # Example for monitoring multiple branches with Merge OR Commit
    #- if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(master|production)$/ || $CI_COMMIT_BRANCH =~ /^(master|production)$/'

TST Plan:
  stage: TST Plan
  script:
    - echo "Run Terraform Plan for Staging"
    - rm -rf terraform*
    # Deploy directory with file for Terraform Plan
    - cd deploy/
    # Terraform init will pull down providers and init backend
    - terraform init
    # Selects Workspace to deploy to (|| = OR) Create Workspace
    - terraform workspace select TST || terraform workspace new TST
    # Plan for staging workspace. Outputs all of the AWS changes to the console logs
    - terraform plan
  rules:
    # Monitor for master|production commits to ensure changes get tested properly
    - if: '$CI_COMMIT_BRANCH == "master"'

TST Apply:
  stage: TST Apply
  script:
    - echo "Run Terraform Apply for Staging"
    - cd deploy/
    - export TF_VAR_ecr_image_api=$ECR_REPO:$CI_COMMIT_SHORT_SHA
    # Terraform init will pull down providers and init backend
    - terraform init
    # Selects Workspace to deploy to
    - terraform workspace select TST
    # Apply for staging workspace. 
    - terraform apply -auto-approve
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: manual  # <-- This makes the job manual; otherwise it would delete right after creation

TST Destroy:
  stage: TST Destroy
  script:
    - echo "Run Terraform Destroy for Staging"
    - cd deploy/
    # Terraform init will pull down providers and init backend
    - terraform init
    # Selects Workspace to deploy to
    - terraform workspace select TST
    # Destroy for staging workspace. 
    - terraform destroy -auto-approve
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: manual  # <-- This makes the job manual; otherwise it would delete right after creation
