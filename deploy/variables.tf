variable "prefix" {
  default = "NM-GITLAB"
}

variable "project" {
  default = "NM GitLab"
}

variable "contact" {
  default = "nickmaize@gmail.com"
}

variable "aws_region" {
  default = "us-east-1"
}

variable "instance_key_name" {
  default = "test_key"
}

variable "vpc_cidr" {
  type = map(any)
  default = {
    TST     = "10.1.0.0/16"
    PROD    = "10.10.0.0/16"
    default = "10.100.0.0/16"
  }
}
# variable "win_username" {}
# variable "win_password" {}
# variable "domain_recovery_password" {}
# ############
# # Gitlab Runner Variables
# ############
# variable "runner_name" {}
# variable "gitlab_url" {}
# variable "registration_token" {}

# variable "gitlab_runner_tags" {
#   type        = string
#   description = "Gitlab Runner tag list (comma separated)."
#   default     = ""
# }

############
# variable "vpc_id" {
#   # default = "vpc-0e6bfc1db49f81b2d" # Correct VPC
#   default = "vpc-028e713921bd78780" # Test with public IP
# }

# variable "subnet_id" {
#   #default = "subnet-0a4f42ba0d8878655" # Correct Subnet
#   default = "subnet-00a7828da1a59ec47" # IGW Subnet
# }
# ###########
# # Combine Subnets and Loop. 2 resources for now, room to optimize code though
# ###########
# # TCI-DEV-UNITS-SN4-INTERNAL-COMMS
# variable "security_group_id01" {
#   default = "sg-01ac0e94500541b2f"
# }
# # TCI-DEV-UNIVERSAL-SG (Unable to add since this is in a different VPC than instance)
# variable "security_group_id02" {
#   default = "sg-0dee913e7c886078a"
# }

# ###########
# # AMIs : Set as variables since AMI IDs change each update
# ###########
# variable "aws_ami_win_base" {
#   default = "ami-00dbe841ffde0d637"
# }

# variable "aws_ami_dc" {
#   default = "ami-0e4820d5323582fc1"
# }

# variable "aws_ami_exchange" {
#   default = "ami-01c1b2526e9b75d9e"
# }

# variable "aws_ami_sharepoint" {
#   default = "ami-0b00e0ae09e584367"
# }

# variable "aws_ami_sql" {
#   default = "ami-0e8b497cfaf13ebde"
# }

# variable "aws_ami_xmpp" {
#   default = "ami-052b0d030d246aed8"
# }

# variable "admin_password" {
#   default = "changeme"
# }