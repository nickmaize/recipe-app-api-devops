terraform {
  # Backend used for state file
  backend "s3" {
    bucket = "nm-s3-terraform-state"
    # Key 
    key     = "nm-demo.tfstate"
    region  = "us-east-1"
    encrypt = true
    # DynamoDB Table: With a remote backend and locking, collaboration is no longer a problem.
    dynamodb_table = "nm-dynamodb-state-lock"
  }
}

# Define which cloud provider
provider "aws" {
  region = "us-east-1"
}

# locals are dynamic variables (variable interpolation) 
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
  # runner_vars = {
  #   gitlab_url   = var.gitlab_url
  #   gitlab_token = var.registration_token
  #   gitlab_tags  = var.gitlab_runner_tags
  # }
}

# This gives a resource value to the current region
data "aws_region" "current" {}