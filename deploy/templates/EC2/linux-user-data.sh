# Tells Linux that this is a bash script
#!/bin/bash

sudo yum update -y
# Install Docker
sudo amazon-linux-extras install -y docker
# Enable Docker
sudo systemctl enable docker.service
# Start Docker
sudo systemctl start docker.service
# usermod command adds EC2 user to the Docker Group
sudo usermod -aG docker ec2-user
