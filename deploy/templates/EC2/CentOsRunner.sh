#!/bin/bash

echo "URL = ${gitlab_url}"

# Update system
sudo yum update -y && \

# Install htop and docker
sudo yum install -y htop docker && \

# Install Gitlab CI Runner
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash && \
sudo yum install gitlab-runner -y

# Enable Services at startup
sudo chkconfig docker on
sudo chkconfig gitlab-runner on

# Register gitlab runner
sudo gitlab-runner register --non-interactive \
      --url "${gitlab_url}" \
      --registration-token "${gitlab_token}" \
      --executor "docker" \
      --tag-list "${gitlab_tags}" \
      --run-untagged="true" \
      --description "docker-runner" \
      --docker-image "hashicorp/terraform:0.13.5" \
      --docker-volumes /var/run/docker.sock:/var/run/docker.sock

# Start services
service docker start
service gitlab-runner start

# Docker system cleanup every 24 hrs to free up space on
echo "0 2 * * * /usr/bin/docker system prune -a -f --filter "until=24h"" >>  /etc/cron.d/docker_cleanup